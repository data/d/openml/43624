# OpenML dataset: Historical_Product_Demand

https://www.openml.org/d/43624

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Source: Charles Gaydon
This data only contains 5 variables of Productcode, Warehouse, ProductCategory, Date, Order_demand
I showed that it is possible, with trivial models, to lower the mean average forecasting error to only around 20 in terms of volume of command, this for 80 of the total volume ordered. This should prove that there is a predicting potential in this dataset that only waits to be exploited.
Again, I the reader wants to continue this work, he or she should use only a selection of the past months to make the forecast.
Other ideas for further development :
-- use warehouse and category data in the model;
-- predict normalized categories of order command (ex: 0 - 1 to 20 - - 100 to 120; where 100 is the historical max of a product) and use a classifier instead of a linear model.
-- check for AIC, BIC,  AICc scores.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43624) of an [OpenML dataset](https://www.openml.org/d/43624). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43624/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43624/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43624/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

